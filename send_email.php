<?php
require("settings.php");

//send_email.php
$file_location = $argv[2];
$to_email_address = $argv[1];

// Pear Mail Library
require_once "Mail.php";

$from = $from_email;
$to = $to_email_address;
$subject = '['.$from_team.'] Credits rendering complete!';
$body = "Your credits are done rendering!  Download them at the link below!\n\n".
    "Right click on the link and click \"Save Link As...\".".
    "  If this option isn't available, open the URL in your browser and press Ctrl+S (or Command+S on a Mac).\n\n".
    $file_location;

$headers = array(
    'From' => $from,
    'To' => $to,
    'Subject' => $subject
);

$smtp = Mail::factory('smtp', array(
        'host' => 'ssl://smtp.gmail.com',
        'port' => '465',
        'auth' => true,
        'username' => $from_email,
        'password' => $from_email_password
    ));

$mail = $smtp->send($to, $headers, $body);

echo "PHP reported:\n\n";
if (PEAR::isError($mail)) {
    echo('ERROR: ' . $mail->getMessage() . '');
} else {
    echo('Message successfully sent!');
}
echo "\n\n";
