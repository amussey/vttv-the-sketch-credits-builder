<?php
require_once("settings.php");
?><html>
<head>
    <title>Credits Builder :: <?=$from_team ?></title>
    <style>
        body {
            font-family: sans-serif;
            color: #fff;
        }
        #credits {
            font-color:#f00;
        }
        .credits {
            font-color:#0f0;
        }
        a {
            color:#fff;
        }
        a:visited {
            color:#fff;
        }
    </style>
</head>
<body bgcolor="#000679">
    <h1>Credits Builder for <?=$from_team ?></h1>
    <img src="images/credits_demo.png" width="50%" />
    <br />
    <br />
    <br />
    <form method="POST" action="render.php" enctype="multipart/form-data">
        <b>Credits information:</b><br />
        <blockquote>
            For multiple people filling one position, use the style:<br />
            <pre>directors=Person A
directors=Person B</pre>
            Anything above the [CAST] title will be converted to lowercase for the roles and uppercase for the Cast.<br />
            Anything below the [CAST] title will be assumed to be a cast member, and will not have the capitalization modified.
        </blockquote>
        <br />
        <textarea id="credits" name="credits" style="width:50%; height:200px;"><?php include "base_credits.txt"; ?></textarea><br />
        <br />
        <br />
        <b>Video file for center video slot (256 MB/60 second limit):</b><br />
        This video should be some sort of outtake, a behind the scenes clip, or a blooper.<br />
        <input type="hidden" name="MAX_FILE_SIZE" value="268435456" />
        <input type="file" name="center_video" id="center_video" /><br />
        <br />
        <br />
        <br />
        <b>Small video (top) (pick one):</b><br />
        <?php
        $videos_raw = trim(file_get_contents("stock_videos/videos.txt"));

        $videos_raw = explode("\n", $videos_raw);
        $videos = array();
        for ($i = 0; $i < count($videos_raw); $i++) {
            if ($videos_raw[$i] != "") {
                $videos[$i] = explode("    ", $videos_raw[$i]);
            }
        }

        $selected_video_1 = rand (0, count($videos)-1);
        $selected_video_2 = rand (0, count($videos)-1);
        while ($selected_video_2 == $selected_video_1) {
            $selected_video_2 = rand (0, count($videos)-1);
        }

        for ($i = 0; $i < count($videos); $i++) {
            echo "<input type=\"radio\" name=\"top_video\" value=\"".$videos[$i][2]."\" ".
                ($i == $selected_video_1 ? "checked" : "")."><img src=\"http://img.youtube.com/vi/".
                $videos[$i][1]."/0.jpg\" width=\"".floor(100/(count($videos)+3))."%\"/>&nbsp;&nbsp;&nbsp;";
        }



        ?><br />
        <br />
        <br />
        <br />
        <b>Small video (bottom) (pick one):</b><br />
        <?php
        for ($i = 0; $i < count($videos); $i++) {
            echo "<input type=\"radio\" name=\"bottom_video\" value=\"".$videos[$i][2]."\" ".
                ($i == $selected_video_2 ? "checked" : "")."><img src=\"http://img.youtube.com/vi/".
                $videos[$i][1]."/0.jpg\" width=\"".floor(100/(count($videos)+3))."%\"/>&nbsp;&nbsp;&nbsp;";
        }
        ?>
        <br />
        <br />
        <br />
        <br />
        <b>Your Email Address (you will get an email when the video is done encoding and ready for download):</b><br />
        <input type="text" name="email_address" id="email_address" value="@vt.edu" /><br />
        Note: We do not store your email.  At all.  Seriously.  We're way too lazy for that.
        <br />
        <br />
        <br />
        <br />
        <input type="submit" name="submit" id="submit" value="Create your credits sequence" />
    </form>
    <br />
    <br />
    <br />
    <br />
    <center style="font-size:11px; color:#fff;">
        This script &copy;<?=date('Y'); ?> Andrew Mussey.  All rights reserved.<br />
        Hosting provided by <a href="http://rackspace.com"  style="color:#fff;">Rackspace</a>.<br />
        Want to edit the source or make your own version?  <a href="https://bitbucket.org/amussey/vttv-the-sketch-credits-builder" style="color:#fff;">Fork me on BitBucket!</a>
    </center>
</body>
</html>
